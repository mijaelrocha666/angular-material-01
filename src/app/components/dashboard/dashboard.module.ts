import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { Formulario01Component } from './formulario01/formulario01.component';
import { Formulario02Component } from './formulario02/formulario02.component';
import { Formulario03Component } from './formulario03/formulario03.component';


@NgModule({
  declarations: [
    DashboardComponent,
    Formulario01Component,
    Formulario02Component,
    Formulario03Component
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
